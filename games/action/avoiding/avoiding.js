"use strict";
const assetsPath = "../../../../mimer-assets-pre/";

class Enemy {
		constructor (graphic, direction)
        {
          	this.direction = direction;
		    this.x=Phaser.Math.Between(0,800);
		    this.y=Phaser.Math.Between(0,600);
          
          	switch(this.direction) {
              	case 0: this.x = 900;break;
                case 1: this.y = -100;break;
                case 2: this.x = -100;break;
                case 3: this.y = 700;break;
            }
		    this.r=Phaser.Math.Between(10,50);
		    this.graphic=graphic;
		    this.speed=Phaser.Math.Between(2,10);
          	this.color = Math.floor(5*Math.random());
          	this.opacity = 1;
		}
		
		update() {
			console.log(this.direction);
          	switch(this.direction) {
              	case 0: this.x-=this.speed;break;
                case 1: this.y+=this.speed;break;
                case 2: this.x+=this.speed;break;
                case 3: this.y-=this.speed;break;
            }
			
          	switch(this.color) {
              	case 0: this.graphic.fillStyle(0x00ff00, 1);break;
                case 1: this.graphic.fillStyle(0x0000ff, 1);break;
                case 2: this.graphic.fillStyle(0xffaa00, 1);break;
                case 3: this.graphic.fillStyle(0xff00ff, 1);break;
                case 4: this.graphic.fillStyle(0x00ffff, 1);break;
            }
			this.graphic.fillCircle(this.x,this.y, this.r);
		}
		
		explode() {
			this.r+=1;
          	this.opacity -= 0.001;
			this.graphic.fillStyle(0xff0000, this.opacity);
			this.graphic.fillCircle(this.x,this.y, this.r);
		}
	}

export class AvoidingScene extends Phaser.Scene {

        constructor (config)
        {
            super(config);
            Phaser.Scene.call(this, { key: "AvoidingScene", active: true });
            this.player;
            this.pen;
            this.cursors;
            this.step=10;
            this.circle=
            this.gameOn=true;
            this.scoreText;
            this.score=0;
          	this.t = 0;
            this.scoreMsg="Score: ";
            this.enemies = [];
            this.killer;
          	this.victoryMessage;
          	this.victoryText;
        }

        preload ()
        {
             this.load.image('player', assetsPath + 'ships/nightraider-224x154.png');
        }

        create ()
        {
             this.player = this.add.image(250, 250, 'player').setScale(0.25);
             this.pen = this.add.graphics();
             this.pen.fillStyle(0x555500, 1);
             this.cursors = this.input.keyboard.createCursorKeys();
             this.scoreText = this.add.text(this.step, this.step,this.scoreMsg+this.score, { fontSize: '32px', fill: '#000' });
          	 
          	 //this.victoryText.alpha = 0;
        }

        update()
        {
			if(this.gameOn) {
			 this.score++;
             this.t++;
		     this.scoreText.setText(this.scoreMsg+this.score); 
	         
	         if (this.cursors.left.isDown)
             {
                 this.player.x-=this.step;
             }
             else if (this.cursors.right.isDown)
             {
                 this.player.x+=this.step;
             }
             else if (this.cursors.up.isDown)
             {
                 this.player.y-=this.step;
             }
             else if (this.cursors.down.isDown)
             {
                 this.player.y+=this.step;
             }
              
             this.player.x = (this.player.x+800)%800
             this.player.y = (this.player.y+600)%600
             
             if(Math.random() < Math.atan(this.t/5000)/10+0.01 && this.t%3000 < 2500) {
				 this.enemies.push(new Enemy(this.pen, Math.floor((this.t%12000)/3000)));
			 }
             
             this.pen.clear();
             for(let i = this.enemies.length-1; i >= 0; i--) {
				//console.log(this.enemies.length);
               	this.enemies[i].update()
				 
				if(Phaser.Math.Distance.Between(this.player.x,this.player.y,this.enemies[i].x,this.enemies[i].y)<this.enemies[i].r){
					this.player.setTint(0x00ff00);
					this.killer=this.enemies[i];
                  	this.killer.r+=20;
					this.scoreText.setStyle( { fontSize: '42px', fill: '#0F0' });
					this.gameOn=false;
			    }
               
               	if(this.enemies[i].x < -150 || this.enemies[i].x > 950 || this.enemies[i].y < -150 || this.enemies[i].y > 750) {
                  this.score += Math.round(this.enemies[i].speed*this.enemies[i].r/10);
                  this.enemies.splice(i, 1);
                }
			 }
        } else {
			this.pen.clear();
			this.killer.explode();
          
          	if(this.score > 10000) {this.player.rotation+=0.1;}
          
          	if(this.score > 20000) {this.victoryMessage = "AMAZING!";}
          	else if(this.score > 15000) {this.victoryMessage = "Astounding";}
          	else if(this.score > 10000) {this.victoryMessage = "Incredible";}
          	else if(this.score > 5000) {this.victoryMessage = "Well done";}
          	else {this.victoryMessage = "";}
            this.victoryText = this.add.text(250, 270, this.victoryMessage, { fontSize: '64px', fill: '#000' });
          	
          	//this.victoryText.alpha = 1;
		}
  
    }
}
