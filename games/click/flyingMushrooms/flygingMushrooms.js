"use strict";
const assetsPath = "../../../../mimer-assets-pre/";

export class FlygingMushrooms extends Phaser.Scene {

        constructor (config)
        {
            super(config);
            Phaser.Scene.call(this, { key: "FlygingMushrooms", active: true });
            this.gameOn=true;
            this.score=0;
            this.scoreMsg="Score: ";
            this.scoreText;
        }
        
        preload() {
			this.load.spritesheet('mushrooms', assetsPath+'icons/mushrooms-diffrent-color-128x128-7x3.png', { frameWidth: 128, frameHeight: 128});
		}

        create ()
        {
		    this.scoreText = this.add.text(10, 10,this.scoreMsg+this.score, { fontSize: '32px', fill: '#FFF' });
		    this.input.on('gameobjectup', function (pointer, gameObject)
            {
                 gameObject.emit('clicked', gameObject);
            }, this);	
        }

        update()
        {
		   if(this.gameOn) {
			   if(Math.random() < 0.22) {
				   let mushroom = this.randomMushroom();
                   let mushroom_image=this.add.image(mushroom.x, mushroom.y, 'mushrooms', mushroom.number).setScale(0.5);
                 let mushroom_images=this.add.image( mushroom.number).setScale(0.5);
                   mushroom_image.setInteractive();
                   
                   if(mushroom.number%7===4) {
					   mushroom_image.on('clicked', this.clickGoodMushroomHandler, this);
				   }
				   else {
					   mushroom_image.on('clicked', this.clickBadMushroomHandler, this);
				   }
			   }
	
	       }
	    }
		
		randomMushroom() {
		    let mushroom={};
		    mushroom.x=Phaser.Math.Between(64,736);
		    mushroom.y=Phaser.Math.Between(64,500);
          	if(Phaser.Math.Between(0,1) == 1){
		    	mushroom.number=Phaser.Math.Between(0,20);
            } else {
              	mushroom.number= 11;
            }
		    return mushroom;
		}
		
		clickGoodMushroomHandler(mushroom)
        {
	        this.score++;
	        this.scoreText.setText(this.scoreMsg+this.score);
            mushroom.off('clicked', this.clickGoodMushroomHandler);
            mushroom.input.enabled = false;
            mushroom.setVisible(false);
            console.log(mushroom);
        }
        
        clickBadMushroomHandler(mushroom)
        {
	        this.score-=2;
	        this.scoreText.setText(this.scoreMsg+this.score);
            mushroom.off('clicked', this.clickGoodMushroomHandler);
            mushroom.input.enabled = false;
            mushroom.setVisible(false);
            console.log(mushroom);
        }
  
    }
