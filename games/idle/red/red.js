"use strict";
const assetsPath = "../../../../mimer-assets-pre/";

export class  IdleRedScene extends Phaser.Scene {

        constructor (config)
        {
            super(config);
            Phaser.Scene.call(this, { key: "IdleREdScene", active: true });
            this.player;
            this.pen;
            this.gameOn=true;
            this.score=0;
            this.energy=0;
            this.scoreMsg= "Score:  ";
            this.energyMsg="Energy: ";
            this.scoreText;
            this.energyText;
            this.goal;
            this.goalBG;
        }

        preload ()
        {
			    this.load.spritesheet('buttons', assetsPath+'buttons/red-click-buttons-200x100-4.png', { frameWidth: 200, frameHeight: 100 });
        }

        create ()
        {
			var row = 100;
            const auto_button = this.add.image(550, row, 'buttons', 0);
            this.add.image(550, row+125, 'buttons', 1);
            this.add.image(550, row+250, 'buttons', 2);
            this.add.image(550, row+375, 'buttons', 3);
            this.scoreText = this.add.text(10, 10,this.scoreMsg+this.score, { fontSize: '32px', fill: '#FF0000' });
            this.energyText = this.add.text(10, 40,this.energyMsg+this.energy, { fontSize: '32px', fill: '#FF0000' });
           
            
            this.pen = this.make.graphics({x: 0, y: 0, add: false});
            this.pen.fillStyle(0xFF0000, 1.0);
            this.pen.fillCircle(90, 90, 80);
            this.pen.generateTexture('RedButtonBG', 200, 400);
            this.goalBG = this.add.image(200, 450, 'RedButtonBG');
            
            this.pen = this.make.graphics({x: 0, y: 0, add: false});
            this.pen.lineStyle(15, 0xFF0000, 1.0);
            this.pen.strokeCircle(90, 90, 80);
            this.pen.generateTexture('RedButton', 200, 400);
            this.goal = this.add.image(200, 450, 'RedButton').setInteractive();
            this.goal.setInteractive();
            
            function addPointsHandler(points) {
				  this.score+=points;
                 if(this.goalBG.alpha < 1) {
                     this.goalBG.alpha+=0.1;
			     }

                 if(this.score%10===0) {
					this.energy++; 
				 }
			}
            
            this.goal.on('pointerdown', () => {
				emitter.emit('addPoints', 1);
             })
             
             var emitter = new Phaser.EventEmitter();
             emitter.on('addPoints', addPointsHandler, this);
             
             auto_button.setInteractive();
             auto_button.on('pointerdown', () => {
				  if(this.energy >= 5) {
					  this.energy-= 5;
                      this.time.addEvent({ delay: 200, callback: function () {emitter.emit('addPoints', 1);}, callbackScope: this, loop: true });
			      }
             })

        }

        update()
        {
			if(this.gameOn) {
				 this.scoreText.setText(this.scoreMsg+this.score);
				 this.energyText.setText(this.energyMsg+this.energy);
				 this.goalBG.alpha -= 0.001;			
		}
  
    }
}
