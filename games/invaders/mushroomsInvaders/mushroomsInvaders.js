"use strict";
const assetsPath = "../../../../mimer-assets-pre/";

let score= 0;
let scoreMsg="Score: ";
let scoreText=null;


export class InvaderScene extends Phaser.Scene {

        constructor (config)
        {
            super(config);
            Phaser.Scene.call(this, { key: "MazeToGoal", active: true });
            this.gameOn=true;
            this.my_buttons = [];
            this.step=6;
            this.player=null; 
            this.spacebar;
            this.invaders = null;
         
        }
        
        preload() {
			this.load.spritesheet('mushrooms', assetsPath + 'icons/mushrooms-diffrent-color-128x128-7x3.png', { frameWidth: 128, frameHeight: 128});
			this.load.image('player', assetsPath + 'ships/raven-128x128.png');
	        this.load.spritesheet('shot', assetsPath + 'particel/many-colors-400x400-9x1.png', { frameWidth: 400, frameHeight: 400});
		}

        create ()
        {
            //For the player
            
			this.player = this.physics.add.image(400, 550, 'player');
			this.player.setScale(0.5).setRotation(-Math.PI/2);
            this.player.setScale(0.5);
						
			this.cursors = this.input.keyboard.createCursorKeys();
			this.spacebar = this.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.SPACE);
			
			// Create a bullet
			//this.pen = this.make.graphics({x: 0, y: 0, add: false});
            //this.pen.fillStyle(0xFF0000, 1.0);
            //this.pen.fillCircle(10, 10, 10, 10);
            //this.pen.generateTexture('bullet', 20, 20);

            //this.pen2 = this.physics.add.image(this.player.x, this.player.y, 'shot');
			//this.pen2.setScale(0.5).setRotation(-Math.PI/2);
            //this.bull = this.physics.add.image(-400, -550, 'shot');
			//this.bull.setScale(0.5).setRotation(-Math.PI/2);
            //this.bull.setScale(0.1);

           //this.bullet = this.physics.add.group({ key: 'shot', frame: 0, repeat: 0, setXY: { x: 0, y: 0, stepX: 60}, setScale: { x: 0.40} });
						

            
			
			//For the invader
            
          	this.invaders = this.physics.add.group({ key: 'mushrooms', frame: 0, repeat: 9, setXY: { x: -600, y: 100, stepX: 60}, setScale: { x: 0.40} });
            this.invaders .setVelocityX(50);
      
            scoreText = this.add.text(16, 16, 'Score: 0', { fontSize: '32px', fill: '#ffffff' });
         }

        update()
        {
          
		  
          if(this.gameOn) {
	         if (this.cursors.left.isDown)
             {
                 this.player.x-=this.step;
             }
             else if (this.cursors.right.isDown)
             {
                 this.player.x+=this.step;
             }
             else if (Phaser.Input.Keyboard.JustDown(this.spacebar))
             {
                 this.shoot();
               	 
             }
             if (this.invaders.x > 100)
             {
                 this.invaders.setVelocityX(-50);
             }
             if (score === 10)
             {
                // work in progress this.invaders .setVelocityX(70);
              game.destroy()
              // fix this this.add.text(500, 500, 'YOU WON!!!', { fontSize: '60px', fill: '#ffffff' }); 
             }
            				
		}
		}
		
		shoot()
        {
            //let bullet = this.physics.add.image(this.player.x, this.player.y, 'shot', setScale:(0.5));
            //this.bullet.setScale(0.5);
            // ny ska vara här this bullet = this.physics.add.group({key: 'shot', frame: 0, repeat: 0, setXY: { x: this.player.x, y: this.player.y, stepX:10}, setScale: {x: 0.40} });
            // fake let bullet = this.physics.add.group({ key: 'shot', frame: 0, repeat: 0, setXY: { x: this.player.x, y: this.player.y, stepX: 10}, setScale: { x: 0.40} });
          	// fake this.invaders = this.physics.add.group({ key: 'mushrooms', frame: 0, repeat: 9, setXY: { x: -600, y: 100, stepX: 60}, setScale: { x: 0.40} });



            //this.bullet.setVelocityY(-400);
            //bullet.body.velocity.y = -400;


           // this.physics.add.overlap(bullet, this.invaders, this.hit, this.score); 






            let bullet = this.physics.add.image(this.player.x, this.player.y, 'shot');
            bullet.body.velocity.y = -400;
            bullet.setScale(0.1);
            this.physics.add.overlap(bullet, this.invaders, this.hit, this.score);












	    }
	    
	    hit(bullet, invader)
        {
            console.log("hit");
            bullet.destroy();
            invader.destroy();
        	score += 1;                           
            scoreText.setText(scoreMsg + score);   
	       
        }
  		
  		
}