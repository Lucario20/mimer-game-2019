"use strict";
const assetsPath = "../../../../mimer-assets-pre/";


export class MazeToGoal extends Phaser.Scene {

        constructor (config)
        {
            super(config);
            Phaser.Scene.call(this, { key: "MazeToGoal", active: true });
            this.gameOn=true;
            this.score=0;
            this.scoreMsg="Score: ";
            this.scoreText;
          	this.timeText;
          	this.gameOverText;
            this.my_buttons = [];
            this.step=4;
        }
        
        preload() {
			this.load.image('maze', assetsPath + 'labyrinth/straight-lines-maze.png');
		}

        create ()
        {
		    this.pen = this.make.graphics({x: 0, y: 0, add: false});
            this.pen.fillStyle(0xFF0000, 1.0);
            this.pen.fillCircle(10, 10, 10, 10);
            this.pen.generateTexture('player', 20, 20);
            this.player = this.add.image(380, 90, 'player');
            
            this.pen = this.make.graphics({x: 0, y: 0, add: false});
            this.pen.fillStyle(0x00FF00, 1.0);
            this.pen.fillRect(0, 0, 30, 30);
            this.pen.generateTexture('goal', 30, 30);
            this.goal = this.add.image(620, 270, 'goal');
            
			this.maze = this.add.image(355, 300, 'maze');
			
			this.scoreText = this.add.text(715, 25,"Time:", { fontSize: '24px', fill: '#000' });
          	this.timeText = this.add.text(715, 45,"", { fontSize: '24px', fill: '#000' });
			this.gameOverText = this.add.text(160,200,"", { fontSize: '100px', fill: '#ff0000' });	
          
			this.cursors = this.input.keyboard.createCursorKeys();
          	this.deathTimer = this.time.addEvent({delay: 1000, repeat: 30});
        }
  

        update()
        {
		  if(this.gameOn) {
             var lastStepX = this.player.x;
             var lastStepY = this.player.y;
            
	         if (this.cursors.left.isDown)
             {
                 this.player.x-=this.step;
             }
             else if (this.cursors.right.isDown)
             {
                 this.player.x+=this.step;
             }
             else if (this.cursors.up.isDown)
             {
                 this.player.y-=this.step;
             }
             else if (this.cursors.down.isDown)
             {
                 this.player.y+=this.step;
             }
			
			let color = this.textures.getPixel(this.player.x, this.player.y, 'maze');
			this.timeText.setText(this.deathTimer.repeatCount);
			if(this.compareAlpha()) {
		       this.player.x = lastStepX;
               this.player.y = lastStepY;
		    }
			if (Phaser.Geom.Intersects.RectangleToRectangle(this.player.getBounds(),this.goal.getBounds() )) {
			    this.scoreText.setText("Winner"); 
              	this.timeText.setText("");
			    this.gameOn = false;	
			}		
            if(this.deathTimer.repeatCount == 0) {
             	this.gameOn = false; 
              	this.scoreText.setText("");
              	this.timeText.setText("");
              	this.gameOverText.setText("GAME OVER");
            }
		}
		}
		
		compareAlpha()
		{
				return this.compareAlphaOnPoint(this.player.getTopLeft()) || 
				       this.compareAlphaOnPoint(this.player.getTopRight())|| 
				       this.compareAlphaOnPoint(this.player.getBottomLeft())|| 
				       this.compareAlphaOnPoint(this.player.getBottomRight());
		}
		
		compareAlphaOnPoint(point)
		{
			return this.textures.getPixel(point.x, point.y, 'maze').alpha === 255;
	    }
  
    }

