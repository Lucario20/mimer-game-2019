"use strict";
const assetsPath = "../../../../mimer-assets-pre/";

export class  PingPong extends Phaser.Scene {

        constructor (config)
        {
            super(config);
            Phaser.Scene.call(this, { key: "PingPong", active: true });
            this.gameOn=true;
            this.score=0;
            this.scoreMsg="Score: ";
            this.scoreText;
            this.previus=-1;
            this.step=1;
            this.cursors=null;
            this.playerOne=null;
            this.ball=null;
        }
        
        preload() {
			this.load.spritesheet('balls', assetsPath+'balls/colored-balls-128x128-9.png', { frameWidth: 128, frameHeight: 128});
			this.load.image('paddelOne', assetsPath+'paddles/paddle-blue-256x64.png');
		}

        create ()
        {
            this.ball=this.physics.add.image(150, 50, 'balls', 1);   
            this.ball.setVelocity(300, 200);
            this.ball.setBounce(1, 1);
            this.ball.setCollideWorldBounds(true);
            
            this.playerOne=this.physics.add.staticImage(50, 300, 'paddelOne');
             this.playerOne.setCollideWorldBounds(true);
            this.cursors = this.input.keyboard.createCursorKeys();
            
             this.scoreText = this.add.text(10, 10,this.scoreMsg+this.score, { fontSize: '32px', fill: '#FFF' });
            this.scoreText.setText(this.scoreMsg+this.score);
        }

        update()
        {
			if(this.gameOn) {
			 this.physics.world.collide(this.ball, this.playerOne);
			 
             if (this.cursors.up.isDown)
             { 
                 this.playerOne.y-=this.step;
             }
             else if (this.cursors.down.isDown)
             {
                 this.playerOne.y+=this.step;
             }
             if(this.ball.x < 100) {
				this.gameOn=false;
				this.ball.setVelocity(0, 0);
				this.add.text(25, 200,"Game Over", { fontSize: '128px', fill: '#F00' });	 
			 }
			 
			 if(this.ball.x > 700) {
				this.score++;
				this.scoreText.setText(this.scoreMsg+this.score);
			 }
			 
			 /*if(this.score > 10) {
				this.gameOn=false;
				this.ball.setVelocity(0, 0);
				this.add.text(25, 200,"Winner", { fontSize: '128px', fill: '#0F0' });	 
			 }*/
             
		 }

	    }
  
    }
