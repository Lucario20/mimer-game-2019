"use strict";
const assetsPath = "../../../../mimer-assets-pre/";

export class MushroomSlalom extends Phaser.Scene {

        constructor (config)
        {
            super(config);
            Phaser.Scene.call(this, { key: "MushroomSlalom", active: true });
            this.gameOn=true;
            this.score=0;
            this.scoreMsg="Score: ";
            this.scoreText;
            this.my_buttons = [];
            this.step=4;
            this.counter;
            this.gates=[];
        }
        
        preload() {
			this.load.spritesheet('mushrooms', assetsPath+ 'icons/mushrooms-diffrent-color-128x128-7x3.png', { frameWidth: 128, frameHeight: 128});
		}
 
        create ()
        {
            this.pen = this.make.graphics({x: 0, y: 0, add: false});
            this.pen.fillStyle(0x00FF00, 1.0);
            this.pen.fillRect(0, 0, 30, 30);
            this.pen.generateTexture('goal', 30, 30);
            this.player = this.add.image(100, 270, 'goal');
            this.scoreText = this.add.text(10, 10,this.scoreMsg+this.score, { fontSize: '32px', fill: '#FFF' });
			this.counter=0;
			
			this.gates[0]=this.createGate((Math.random() * 500));;
			
			this.cursors = this.input.keyboard.createCursorKeys();
        }

        update()
        {
		  if(this.gameOn) {
			 this.counter++;
			 if(this.counter % 200 === 0) { 
			     this.gates.push(this.createGate(Math.random() * 500));
		     }
	         if (this.cursors.up.isDown)
             {
                 this.player.y-=this.step;
             }
             else if (this.cursors.down.isDown)
             {
                 this.player.y+=this.step;
             }
		 
             if(this.player.x-5 < this.gates[0].left.x && this.gates[0].left.x < this.player.x+5) {
				 if(this.gates[0].left.y < this.player.y && this.player.y < this.gates[0].right.y) {
					 this.score+=1; 
                   console.log("Hit");
                    this.scoreText = this.add.text(10, 10,this.scoreMsg+this.score, { fontSize: '32px', fill: '#FFF' });
                        
                  
					 this.gates.shift();
					 
				 }
				 else {
					 this.gameOn=false;
					 this.add.text(25, 200,"Game Over", { fontSize: '128px', fill: '#F00' });
					 console.log("Boom");
				 }
			 }
							
		}
		}
		
		createGate(StartY) {
			let gate={};
			gate.left = this.add.image(700,StartY, 'mushrooms', 1).setScale(0.25);
            gate.right = this.add.image(700, StartY+100, 'mushrooms', 1).setScale(0.25);
            gate.tween = this.tweens.add({
                targets: [gate.left, gate.right] ,
                x: -50,
                //ease: 'Power1',
                duration: 5000
            });
            return gate;
		}
  
    }
